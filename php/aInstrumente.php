<div style="margin-bottom:10px;">
	<button id="new"  class="btn btn-primary">Utensil hinzufügen</button>	
</div>
<div id="anlegen">
	<input type="text" placeholder="Instrument" id="iName"></input>
	<button id="iAnlegen" class="btn btn-success">Hinzufügen</button>
</div>
<div id="iContent">
<hr style="margin-bottom:10px;">
<?php
include('pdo.php');
$res = $db->query('select * from instrument');
$res2 = $res->fetchAll(PDO::FETCH_ASSOC);
foreach($res2 as $row){
	?>
		<div id="<?php echo $row['i_id']; ?>" style="margin-bottom:0px;">
			<span><?php echo $row['beschreibung']; ?></span>
			<div style="float:right;">
				<button id="<?php echo 'e'.$row['i_id']; ?>" class="btn btn-warning">Editieren</button>
				<button id="<?php echo 'd'.$row['i_id']; ?>" class="btn btn-warning">Löschen</button>
		</div>
		
		
		<div id="edit<?php echo $row['i_id']; ?>">
			<input type="text" id="eName<?php echo $row['i_id']; ?>" value="<?php echo $row['beschreibung']; ?>"></input>
			<button id="eAnlegen<?php echo $row['i_id']; ?>" class="btn btn-success">Hinzufügen</button>
		</div>
		<hr style="margin-top:15px;margin-bottom:10px;">
		</div>
		<script>$('#<?php echo 'e'.$row['i_id'];?>').click(function () {edit(	<?php echo $row['i_id']; ?>);});
		$('#<?php echo 'd'.$row['i_id'];?>').click(function () {löschen(<?php echo $row['i_id']; ?>);});
		$('#edit<?php echo $row['i_id']; 	?>').hide();
		$('#eAnlegen<?php echo $row['i_id']; 	?>').click(function () {iedit(	<?php echo $row['i_id']; ?>);});</script>
	<?php
}
?>
</div>
<script>
//Hinzufügen zu beginn
$('#anlegen').hide();
$('#new').click(function () {neu();});
$('#iAnlegen').click(function () {anlegen();});
//Editieren
$('#edit')
function anlegen(){
	$.ajax({
			url:"http://localhost/musik/php/iHinzu.php",
			type: "POST",
			data: {iBeschreibung: $('#iName').val()}
				}).done(function (data){
					instrumente();
				});
}
function neu(){
	$('#anlegen').show();
	$('#new').hide();
}

//einzelne Zeilen
function edit(i_id){
	$('#edit'+i_id).show();
}
function iedit(i_id){
	var beschreibung = $('#eName'+i_id).val();
	$.ajax({
			url:"http://localhost/musik/php/iEdit.php",
			type: "POST",
			data: {i_id: i_id, beschreibung: beschreibung}
				}).done(function (data){
					instrumente();
				});
}
function löschen(i_id){
	$.ajax({
			url:"http://localhost/musik/php/iDelete.php",
			type: "POST",
			data: {i_id: i_id}
				}).done(function (data){
					$('#'+i_id).remove();
				});
}
</script>