<?php
include('pdo.php');
	$res = $db->query('SELECT * FROM konzert');
	$res2 = $res->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="kContainter">
	<div>
		<button id="newK" class="btn btn-primary">Neues Konzert</button>
	</div>
<?php	
	foreach($res2 as $konzert){
			?><div style="height:100px;">
				<div style="float:left;">
					<h1><?php echo $konzert['titel']; ?></h1>
					</p> am <?php echo $konzert['datum']; ?></p>
				</div>
				<div style="margin-left:20px; float:left;">
					</p><?php echo $konzert['beschreibung']; ?></p>
				</div>
				<button id="deleteK<?php echo $konzert['k_id']; ?>" style="float:right; margin-left:5px;" class="btn btn-warning">Löschen</button>
				<button id="editK<?php echo $konzert['k_id']; ?>" style="float:right" class="btn btn-warning">Editieren</button>
				<script>
					$('#deleteK<?php echo $konzert['k_id']; ?>').click(function () { deleteK(<?php echo $konzert['k_id']; ?>); });
					$('#editK<?php echo $konzert['k_id']; ?>').click(function () { editK(<?php echo $konzert['k_id']; ?>); });
				</script>
			</div><hr><?php
	}
?>
</div>
<script>
$('#newK').click(function () { neuesKonzert(); });
$('#deleteK').click(function () { deleteK(); });
$('#editK').click(function () { editK(); });

function deleteK(ID){
$.ajax({
	url:"http://localhost/musik/php/aKonzertDelete.php",
	type: "POST",
	data: {k_id: ID}
	}).done(function (data){
		konzerte();
	});
}
function editK(ID){
	$('#kContainter').empty();
	$.ajax({
	url:"http://localhost/musik/php/aKonzertEdit.php",
	type: "POST",
	data: {k_id: ID}
	}).done(function (data){
		$('#kContainter').append(data);
	});
}
function neuesKonzert(){
	$('#kContainter').empty();
	$.ajax({
			url:"http://localhost/musik/php/aKonzertneu.php",
				}).done(function (data){
					$('#kContainter').append(data);
				});
}
</script>