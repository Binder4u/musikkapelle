<?php
include('pdo.php');
?>
<div>
	<button id="new" class="btn btn-primary">Mitglied hinzufügen</button>
</div>
<div id="anlegenInput">
	<input type="text" placeholder="Vorname" id="mVN"></input>
	<input type="text" placeholder="Nachname" id="mNN"></input>
	<select name="Instrument" id="mI">
	<?php
		$res = $db->query('select * from instrument');
		$res2 = $res->fetchAll(PDO::FETCH_ASSOC);
		foreach($res2 as $row){
			echo '<option value=\''.$row['i_id'].'\'>'.$row['beschreibung'].'</option>';
		}
	?>	
	</select>
	<button id="mAnlegenButton" class="btn btn-success">Hinzufügen</button>
</div>
<hr style="margin-top:20px;margin-bottom:10px;">
<?php
$res = $db->query('select m.m_id,m.vorname, m.nachname, i.beschreibung from mitglieder m , instrument i where i.i_id = m.i_id');
$res2 = $res->fetchAll(PDO::FETCH_ASSOC);
foreach($res2 as $row){
?><div id="<?php echo $row['m_id']; ?>" style="margin-bottom:0px;">
			<div class="row">
			<span class="col-md-2" style="margin-right:20px;"><?php echo $row['vorname']; ?></span>
			<span class="col-md-2" style="margin-right:20px;"><?php echo $row['nachname']; ?></span>
			<span class="col-md-2" style="margin-right:20px;"><?php echo $row['beschreibung']; ?></span>
				<div style="float:right;">
					<button id="<?php echo 'eButton'.$row['m_id']; ?>" class="btn btn-warning">Editieren</button>
					<button id="<?php echo 'dButton'.$row['m_id']; ?>" class="btn btn-warning">Löschen</button>
				</div>
			</div>
			<div id="edit<?php echo $row['m_id']; ?>">
				<input type="text" id="eVN<?php echo $row['m_id']; ?>" value="<?php echo $row['vorname']; ?>"></input>
				<input type="text" id="eNN<?php echo $row['m_id']; ?>" value="<?php echo $row['nachname']; ?>"></input>
				<select name="EditInstrument" id="eI">
				<?php
					$instrumente = $db->query('select * from instrument');
					$instrumente2 = $instrumente->fetchAll(PDO::FETCH_ASSOC);
					foreach($instrumente2 as $row2){
						echo '<option value=\''.$row2['i_id'].'\'>'.$row2['beschreibung'].'</option>';
					}
				?>	
				</select>
				<button id="eAnlegen<?php echo $row['m_id']; ?>" class="btn btn-success">Ändern</button>
			</div>
			<script>
				$('#dButton<?php 	echo $row['m_id']; ?>').click(function () {deleteButton(<?php echo $row['m_id']; ?>);});
				$('#eButton<?php 	echo $row['m_id']; ?>').click(function () {editButton(<?php echo $row['m_id']; ?>);});
				$('#edit<?php 		echo $row['m_id']; ?>').hide();
				$('#eAnlegen<?php 	echo $row['m_id']; ?>').click(function () {editAnlegen(<?php echo $row['m_id']; ?>);});
			</script>
	<hr style="margin-top:15px;margin-bottom:10px;">
</div>

<?php
}
?>

<script>
$('#anlegenInput').hide();

$('#new').click(function (){$('#anlegenInput').show();$('#new').hide();});
$('#mAnlegenButton').click(function () {neuesMitglied();});


function neuesMitglied(){
	var vn = $('#mVN').val();
	var nn = $('#mNN').val();
	var mI = $('#mI').val();
	$.ajax({
			url:"http://localhost/musik/php/aNeuMitglied.php",
			type: "POST",
			data: {vn: vn, nn: nn, mI: mI}
				}).done(function (data){
					mitglieder();
				});
}
function deleteButton(m_id){
	$.ajax({
			url:"http://localhost/musik/php/aDelete.php",
			type: "POST",
			data: {m_id: m_id}
				}).done(function (data){
					$('#'+m_id).remove();
				});
}
function editButton(m_id){
	$('#edit'+m_id).show();
}
function editAnlegen(m_id){
	var eVN = $('#eVN'+ m_id).val();
	var eNN = $('#eNN'+ m_id).val();
	var Instrument = $('#eI').val();
	//alert(eVN+eNN+Instrument);
	$.ajax({
			url:"http://localhost/musik/php/aMitgliederEdit.php",
			type: "POST",
			data: {m_id: m_id, eVN: eVN, eNN: eNN, Instrument: Instrument}
				}).done(function (data){
					mitglieder();
				});
}
</script>