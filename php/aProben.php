<?php
include('pdo.php');
if(isset($_POST['m_id'])){
	$m_id = $_POST['m_id'];
?>
<div id="pContainer">
	<button style="clear:left;" id="newProbe" class="btn btn-primary" style="float:right;margin-left:5px;">Probe erstellen</button><hr>
	<script>
	var m_id = <?php echo $m_id; ?>;
	$('#newProbe').click(function () {
		neueProbe();
	});
	</script> 
<?php
	$res = $db->query('SELECT * FROM proben');
	$res2 = $res->fetchAll(PDO::FETCH_ASSOC);

	foreach($res2 as $proben){
			?><div style="margin-bottom:7px;">
				<span>Probe am <?php echo $proben['Datum'];?></span>
				<span style="margin-left:20px;"><?php echo $proben['beschreibung'];?></span>
				<button id="delete<?php echo $proben['p_id']; ?>" class="btn btn-warning" style="float:right;margin-left:5px;">Löschen</button>
				<button id="edit<?php echo $proben['p_id']; ?>" class="btn btn-warning" style="float:right;margin-left:5px;">Editieren</button>
				<button id="anzeigen<?php echo $proben['p_id']; ?>" class="btn btn-warning" style="float:right;">Teilnehmer</button>
			</div>
			<hr>
			<script>$('#delete<?php echo $proben['p_id']; ?>').click(function () {deleteProbe(<?php echo $proben['p_id']; ?>);});</script>
			<script>$('#edit<?php echo $proben['p_id']; ?>').click(function () {editProbe(<?php echo $proben['p_id']; ?>);});</script>
			<script>$('#anzeigen<?php echo $proben['p_id']; ?>').click(function () {teilnehmerAnzeigen(<?php echo $proben['p_id']; ?>);});</script>
			<?php
	}
	?>
</div>
	<script>
		function deleteProbe(probeId){
			$.ajax({
			url:"http://localhost/musik/php/aProbenDelete.php",
			type: "POST",
			data: {p_id: probeId}
				}).done(function (data){
					proben();
				});
		}
		function editProbe(probeId){
			$.ajax({
			url:"http://localhost/musik/php/aProbenEdit.php",
			type: "POST",
			data: {p_id: probeId}
				}).done(function (data){
					$('#pContainer').empty();
					$('#pContainer').append(data);
				});
		}
		function neueProbe(){
			$.ajax({
			url:"http://localhost/musik/php/aProbenNeu.php",
				}).done(function (data){
					$('#pContainer').empty();
					$('#pContainer').append(data);
				});
		}
		function teilnehmerAnzeigen(probeId){
			$.ajax({
			url:"http://localhost/musik/php/aProbenMitglieder.php",
			type: "POST",
			data: {p_id: probeId}
				}).done(function (data){
					$('#pContainer').empty();
					$('#pContainer').append(data);
				});
		}
	</script>
	<?php
}
?>